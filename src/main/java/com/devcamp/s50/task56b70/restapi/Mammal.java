package com.devcamp.s50.task56b70.restapi;

public class Mammal extends Animal{
     public Mammal(String name) {
          super(name);
      }
  
      public String toString() {
          return "Mammal[" + super.toString() + "]";
      }
}
