package com.devcamp.s50.task56b70.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class AnimalController {
     @GetMapping("/cats")
     public ArrayList<Cat> getCats(){
          ArrayList<Cat> cats = new ArrayList<>();

          Cat cat1 = new Cat("Myo");
          Cat cat2 = new Cat("Lum");
          Cat cat3 = new Cat("Miu");

          System.out.println(cat1.toString());
          System.out.println(cat2.toString());
          System.out.println(cat3.toString());

          cats.add(cat1);
          cats.add(cat2);
          cats.add(cat3);
          return cats;
     }
    
}
