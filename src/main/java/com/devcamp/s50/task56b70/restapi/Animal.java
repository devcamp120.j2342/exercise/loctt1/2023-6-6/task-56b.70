package com.devcamp.s50.task56b70.restapi;

public class Animal {
     private String name;

     public Animal(String name) {
          this.name = name;
     }

     public String getnName(){
          return name;
     }

     @Override
     public String toString() {
          return "Animal [name=" + this.name + "]";
     }

     
}
