package com.devcamp.s50.task56b70.restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@CrossOrigin
public class DogController {
     @GetMapping("/dogs")
     public ArrayList<Dog> getDogs() {
         ArrayList<Dog> dogs = new ArrayList<>();

         Dog dog1 = new Dog("Den");
         Dog dog2 = new Dog("vang");
         Dog dog3 = new Dog("Xam");

         System.out.println(dog1.toString());
         System.out.println(dog2.toString());
         System.out.println(dog3.toString());

         dogs.add(dog1);
         dogs.add(dog2);
         dogs.add(dog3);
         return dogs;
     }
}
